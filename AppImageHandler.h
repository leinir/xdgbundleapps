/*
 *   SPDX-FileCopyrightText: 2021 Dan Leinir Turthra Jensen <admin@leinir.dk>
 *
 *   SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#ifndef APPIMAGEHANDLER_H
#define APPIMAGEHANDLER_H

#include "BundleHandler.h"

#include <QRunnable>

class AppImageHandler : public BundleHandler
{
    Q_OBJECT
public:
    static BundleHandler* getInstanceForBundle(const QString &filename);
    explicit AppImageHandler(QObject *parent = 0);
    ~AppImageHandler() override;

    void start() override;
private:
    class Private;
    std::unique_ptr<Private> d;
};

class AppImageRunner : public QObject, public QRunnable {
    Q_OBJECT
public:
    enum Error {
        AppimagedUnknownError = 1, ///< There is a problem with appimaged that we don't understand
        AppimagedDownloadError = 2, ///< We failed to download appimaged
        AppimagedInstallationError = 3, ///< We could not set up appimaged
        CopyError = 101, ///< We could not copy the appimage into its new home
        NotDowngradingError = 102, ///< The appimage we were requested to install is older than the one that is already in the Applications directory
        LaunchError = 201, ///< We failed at launching the appimage
        RemoveError = 301, ///< We failed to delete the appimage
    };
    explicit AppImageRunner(const QString &appimage, bool launch, bool allowDowngrade, bool remove);
    ~AppImageRunner() override;

    void run() override;
    Q_SIGNAL void done();
    Q_SIGNAL void error(AppImageRunner::Error error);
private:
    class Private;
    std::unique_ptr<Private> d;
};

#endif//APPIMAGEHANDLER_H
