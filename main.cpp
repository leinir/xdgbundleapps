/*
 *   SPDX-FileCopyrightText: 2021 Dan Leinir Turthra Jensen <admin@leinir.dk>
 *
 *   SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include "BundleHandler.h"
#include "XHABVersion.h"

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QFile>

#include <KAboutData>
#include <KCrash>
#include <KLocalizedString>

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    KCrash::initialize();
    KLocalizedString::setApplicationDomain("xdg-handle-app-bundle");
    KAboutData about(QStringLiteral("xdg-handle-app-bundle"), i18n("XDG Install Bundle App"), XIBA_VERSION_STRING, i18n("Installation helper for bundled apps"),
                     KAboutLicense::GPL, i18n("© 2010-2016 Plasma Development Team"));
    about.addAuthor(i18n("Dan Leinir Turthra Jensen"), QString(), QStringLiteral("admin@leinir.dk"), QStringLiteral("https://leinir.dk"), QStringLiteral("leinir"));
    about.setProductName("xdg-handle-app-bundle");
    KAboutData::setApplicationData(about);

    // NB: deliberately no magic here, this app is not really supposed to be pluginifiable, as we want to keep it as simple as possible
    QStringList supportedFormats;
    supportedFormats << "appimage";
    supportedFormats << "flatpakref";
    supportedFormats << "snap";

    QCommandLineParser* parser = new QCommandLineParser;
    parser->addPositionalArgument("packagefile", i18n("The local filename for the package you wish to handle. The default operation is to install the bundle on your system. Supported formats are: %1").arg(supportedFormats.join(QLatin1Char(','))));
    parser->addHelpOption();
    parser->setApplicationDescription(i18n("This tool is designed to handle bundle applications in various formats, with a deliberately minimal amount of interaction from the user. The default operation is to install the bundle onto your system (or upgrade an existing bundle if an older version exists), but you can also set various flags to launch the bundle after installing it, as well as remove it. It is not supposed to have granular support for everything the various bundle formats support; if such functionality is required, use the bundle format's own tools to perform those operations."));
    parser->addOption(QCommandLineOption("ui-progress", i18n("Show progress using your desktop environment's notification system.")));
    parser->addOption(QCommandLineOption("cli-progress", i18n("Show progress on the command line")));
    parser->addOption(QCommandLineOption("launch", i18n("Attempt to launch the bundle after installing it (if it is already installed, it will simply be launched)")));
    parser->addOption(QCommandLineOption("allow-downgrade", i18n("Install this bundle, even if the currently installed version is newer than this one")));
    parser->addOption(QCommandLineOption("remove", i18n("Pass this option to remove the bundle from the system")));
    parser->process(app);

    // Basic logic concept:
    // First work out which format the package is by suffix(/mimetype?)
    // Create an instance of the appropriate BundleHandler subclass
    // Set appropriate options (which file, progressification option)
    // Initiate installation (checking if already installed, silently succeed if already installed, update if newer than installed, fail angrily if attempting to downgrade)
    // If desired, after successful installation, also run the app (handler does this)

    if (parser->positionalArguments().length() > 0) {
        QString packagefile = parser->positionalArguments().first();
        if(!QFile::exists(packagefile)) {
            qWarning() << "The package file does not exist. The file passed in was:" << packagefile;
            return -2;
        }

        BundleHandler* handler = BundleHandler::getInstanceForBundle(packagefile);
        if(!handler) {
            qWarning() << "The package file is not recognised. The supported package formats are" << supportedFormats.join(QLatin1Char(',')) << "and the file passed in was:" << packagefile;
            return -3;
        }

        QObject::connect(handler, &BundleHandler::processCompleted, &app, &QCoreApplication::quit);

        handler->setShowProgress(parser->isSet("ui-progress"));
        handler->setShowCliProgress(parser->isSet("cli-progress"));
        handler->setLaunch(parser->isSet("launch"));
        handler->setAllowDowngrade(parser->isSet("allow-downgrade"));
        handler->setRemove(parser->isSet("remove"));
        handler->start();
    } else {
        qWarning() << "No package file was passed in.";
        parser->showHelp(-1);
    }

    return app.exec();
}
