/*
 *   SPDX-FileCopyrightText: 2021 Dan Leinir Turthra Jensen <admin@leinir.dk>
 *
 *   SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include "AppImageHandler.h"

#include <QCoreApplication>
#include <QCryptographicHash>
#include <QDebug>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QProcess>
#include <QMutexLocker>
#include <QNetworkReply>
#include <QStandardPaths>
#include <QStorageInfo>
#include <QThreadPool>
#include <QTimer>

#include <processcore/processes.h>
#include <processcore/process.h>

class AppImageHandler::Private
{
public:
    Private() {}
};

BundleHandler* AppImageHandler::getInstanceForBundle(const QString &filename)
{
    BundleHandler* handler = 0;
    // if we can handle filename, return an instance, otherwise return null
    if(filename.toLower().endsWith(".appimage")) {
        handler = new AppImageHandler();
    }
    return handler;
}

AppImageHandler::AppImageHandler(QObject* parent)
    : BundleHandler(parent)
    , d(new Private)
{
    qRegisterMetaType<AppImageRunner::Error>("AppImageRunner::Error");
}

AppImageHandler::~AppImageHandler()
{
}

void AppImageHandler::start()
{
    AppImageRunner *runner = new AppImageRunner(bundleFilename(), launch(), allowDowngrade(), remove());
    connect(runner, &AppImageRunner::done, this, &BundleHandler::processCompleted);
    connect(runner, &AppImageRunner::error, this, [this](AppImageRunner::Error error){
        switch(error)
        {
            case AppImageRunner::AppimagedUnknownError:
                break;
            case AppImageRunner::AppimagedDownloadError:
                break;
            case AppImageRunner::AppimagedInstallationError:
                break;
            case AppImageRunner::CopyError:
                break;
            case AppImageRunner::NotDowngradingError:
                break;
            default:
                break;
        }
    });
    QThreadPool::globalInstance()->start(runner);
}

class HTTPWorkerNAM
{
public:
    HTTPWorkerNAM()
    {
        QMutexLocker locker(&mutex);
        const QString cacheLocation = QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + QStringLiteral("/knewstuff");
        cache.setCacheDirectory(cacheLocation);
        QStorageInfo storageInfo(cacheLocation);
        cache.setMaximumCacheSize(qMin(50 * 1024 * 1024, (int)(storageInfo.bytesTotal() / 1000)));
        nam.setCache(&cache);
    }
    QNetworkAccessManager nam;
    QMutex mutex;

    QNetworkReply *get(const QNetworkRequest &request)
    {
        QMutexLocker locker(&mutex);
        return nam.get(request);
    }

private:
    QNetworkDiskCache cache;
};
Q_GLOBAL_STATIC(HTTPWorkerNAM, s_httpWorkerNAM)

class AppImageRunner::Private {
public:
    Private(AppImageRunner *q)
        : q(q)
    {
        applicationsLocation = QLatin1String("%1/Applications").arg(QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first());
        appimagedLocation = QString("%1/%2").arg(applicationsLocation).arg(QStringLiteral("appimaged.AppImage"));
    }
    AppImageRunner *q;
    QString applicationsLocation;
    QString appimagedLocation;
    QString appimage;
    bool launch{false};
    bool allowDowngrade{false};
    bool remove{false};

    QNetworkReply *reply{nullptr};
    QUrl redirectUrl;
    QFile dataFile;
    bool isFinished{true};
    void handleData()
    {
        qDebug() << Q_FUNC_INFO;
        QMutexLocker locker(&s_httpWorkerNAM->mutex);
        if (reply->attribute(QNetworkRequest::RedirectionTargetAttribute).isNull()) {
            if (!dataFile.isOpen()) {
                if (dataFile.open(QIODevice::WriteOnly)) {
                    // All is well
                } else {
                    qWarning() << "Failed to open" << dataFile.fileName() << "for writing!";
                }
            }
            do {
                dataFile.write(reply->read(32768));
            } while (!reply->atEnd());
        }
    }
    void handleFinished()
    {
        // Handle redirections
        const QUrl possibleRedirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        if (!possibleRedirectUrl.isEmpty() && possibleRedirectUrl != redirectUrl) {
            redirectUrl = reply->url().resolved(possibleRedirectUrl);
            if (redirectUrl.scheme().startsWith(QLatin1String("http"))) {
//                 qDebug() << reply->url().toDisplayString() << "was redirected to" << redirectUrl.toDisplayString() << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
                reply->deleteLater();
                QNetworkRequest request(redirectUrl);
                reply = s_httpWorkerNAM->get(request);
                connect(reply, &QNetworkReply::readyRead, q, [this](){ handleData(); });
                connect(reply, &QNetworkReply::finished, q, [this](){ handleFinished(); });
                return;
            } else {
                qWarning() << "Redirection to" << redirectUrl.toDisplayString() << "forbidden.";
            }
        } else {
            qDebug() << "Data for" << reply->url().toDisplayString() << "was fetched";
            dataFile.close();
            dataFile.setPermissions(QFile::ReadOwner | QFile::WriteOwner | QFile::ExeUser | QFile::ExeGroup| QFile::ExeOwner | QFile::ExeOther);
            isFinished = true;
        }
    }

    bool checkAppimaged()
    {
        bool success{true};
        // Check whether appimaged is already running, at which point all is well
        KSysGuard::Processes *processes = new KSysGuard::Processes();
        processes->updateAllProcesses();
        const QList<KSysGuard::Process *> processlist = processes->getAllProcesses();
        for (const KSysGuard::Process *process : processlist) {
            if (process->command().contains("appimaged")) {
                // Just because we can't reeeally trust that it necessarily is going to be where we think, so...
                appimagedLocation = process->command().trimmed().split(" ").first();
                success = true;
                break;
            }
        }
        delete processes;
        if (success) {
            qDebug() << "Nice, that's us with an already set up appimaged, let's gooooo";
        } else {
            // If it's not running, check if we've got it where we think it should be
            if (!QFileInfo::exists(appimagedLocation)) {
                // If we don't, download it to there and make it executable
                QString arch;
                if (QSysInfo::currentCpuArchitecture() == QLatin1String("arm")) {
                    arch = QLatin1String("armhf");
                } else if (QSysInfo::currentCpuArchitecture() == QLatin1String("arm64")) {
                    arch = QLatin1String("aarch64");
                } else if (QSysInfo::currentCpuArchitecture() == QLatin1String("i386")) {
                    arch = QLatin1String("i686");
                } else if (QSysInfo::currentCpuArchitecture() == QLatin1String("ia64")) {
                    arch = QLatin1String("x86_64");
                } else if (QSysInfo::currentCpuArchitecture() == QLatin1String("x86_64")) {
                    arch = QLatin1String("x86_64");
                }

                // get with header "Accept: application/vnd.github.v3+json" https://api.github.com/repos/probonopd/go-appimage/releases/latest
                QNetworkRequest request = QNetworkRequest(QUrl("https://api.github.com/repos/probonopd/go-appimage/releases/latest"));
                request.setRawHeader("Accept", "application/vnd.github.v3+json");
                reply = s_httpWorkerNAM->get(request);
                // Let's just do this sequentially (because we already do this in a runner, no need to spread our threads any thinner)
                isFinished = false;
                connect(reply, &QNetworkReply::finished, q, [this](){ isFinished = true; });
                while (!isFinished) {
                    qApp->processEvents();
                }

                if (reply->error() == QNetworkReply::NoError) {
                    QByteArray releaseData = reply->readAll();
                    QJsonDocument releaseDoc = QJsonDocument::fromJson(releaseData);
                    QJsonObject releaseObject = releaseDoc.object();
                    // iterate over the assets children, find the one where name starts with appimage and ends with ${arch}.AppImage
                    QString assetDownloadUrl;
                    if (releaseObject.contains("assets") && releaseObject["assets"].isArray()) {
                        QJsonArray assets = releaseObject["assets"].toArray();
                        static const QLatin1String assetNameStart{"appimaged-"};
                        static const QString assetNameEnds = QLatin1String("-%1.AppImage").arg(arch);
                        for (QJsonArray::const_iterator i = assets.constBegin(); i != assets.constEnd(); ++i) {
                            QJsonObject obj = i->toObject();
                            if (obj.contains("name")) {
                                const QString assetName = obj.value("name").toString();
                                if (assetName.startsWith(assetNameStart) && assetName.endsWith(assetNameEnds)) {
                                    assetDownloadUrl = obj["browser_download_url"].toString();
                                    break;
                                }
                            }
                        }
                    } else {
                        // no assets in the release, this seems kind of weird...
                    }
                    if (assetDownloadUrl.isEmpty()) {
                        // Failed to find a download url, not awesome, we can't continue
                    } else {
                        qDebug() << "Downloading" << assetDownloadUrl;
                        // download the thing at "browser_download_url" to appimagedLocation
                        request = QNetworkRequest(QUrl(assetDownloadUrl));
                        reply = s_httpWorkerNAM->get(request);
                        dataFile.setFileName(appimagedLocation);
                        connect(reply, &QNetworkReply::readyRead, q, [this](){ handleData(); });
                        connect(reply, &QNetworkReply::finished, q, [this](){ handleFinished(); });
                        // Let's just do this sequentially (because we already do this in a runner, no need to spread our threads any thinner)
                        isFinished = false;
                        while (!isFinished) {
                            qApp->processEvents();
                        }

                        // If it still isn't there, exit stage left
                        if (!QFileInfo::exists(appimagedLocation)) {
                            success = false;
                            Q_EMIT q->error(AppimagedDownloadError);
                        }
                    }
                } else {
                    // Failed to get the appimaged release info - why? and tell the user...
                }
            }
            if (success) {
                // Run appimaged
                QProcess appimaged;
                appimaged.start(appimagedLocation, QStringList{});
                appimaged.waitForFinished();
                if (appimaged.exitCode() != 0) {
                    qCritical() << appimaged.readAll();
                    success = false;
                    Q_EMIT q->error(AppimagedInstallationError);
                }
            }
        }
        return success;
    }
};

AppImageRunner::AppImageRunner(const QString& appimage, bool launch, bool allowDowngrade, bool remove)
    : d(new Private(this))
{
    d->appimage = appimage;
    d->launch = launch;
    d->allowDowngrade = allowDowngrade;
    d->remove = remove;
}

AppImageRunner::~AppImageRunner()
{
}

void AppImageRunner::run()
{
    QFile originLocation{d->appimage};
    if (d->remove) {
        // Simply delete the file - report error if unsuccessful (we assume a file passed here is the canonical installed
        // location - arguably we could introspect, but that is not really how appimages are supposed to work)
        if (originLocation.exists()) {
            if (!originLocation.remove()) {
                Q_EMIT error(RemoveError);
            }
        } else {
            // The file isn't there, maybe spit out a warning? Not really an error, but...
        }
    } else {
        const QString installedLocation = QLatin1String("%1/%2").arg(d->applicationsLocation).arg(originLocation.fileName().split("/").last());
        // Make sure appimaged is a thing
        if (d->checkAppimaged()) {
            // now check whether this appimage is already in the user's $HOME/Applications dir and if not, move it there for appimaged to pick up
            bool shouldContinue{true};
            if (d->appimage == installedLocation) {
                // Simplest case - we're just running a file that's already where it's supposed to end up, all is well, just run it
                shouldContinue = true;
            } else {
                if (QFileInfo::exists(installedLocation)) {
                    // First check if the files are identical - quick SHA-1 hash on both will tell us this
                    QCryptographicHash originHash(QCryptographicHash::Sha1);
                    QCryptographicHash destinationHash(QCryptographicHash::Sha1);
                    if (originLocation.open(QIODevice::ReadOnly)) {
                        originHash.addData(&originLocation);
                        originLocation.close();
                    }
                    QFile destinationFile(installedLocation);
                    if (destinationFile.open(QIODevice::ReadOnly)) {
                        destinationHash.addData(&destinationFile);
                        destinationFile.close();
                    }
                    QByteArray sigOrigin = originHash.result();
                    QByteArray sigDestination = destinationHash.result();
                    if (sigOrigin == sigDestination) {
                        shouldContinue = true;
                    } else {
                        // if it's already there, check the version (overwrite if newer, ignore if same, replace if downgrade permitted)
                        bool shouldReplace{false};
                        if (d->allowDowngrade) {
                            shouldReplace = true;
                        } else {
                            ///TODO file exists, and we're not allowed to downgrade, so try and work out the version first
                        }
                        if (shouldReplace) {
                            QFile::remove(installedLocation);
                            originLocation.copy(installedLocation);
                        } else {
                            shouldContinue = false;
                        }
                    }
                } else {
                    originLocation.copy(installedLocation);
                }
            }
            if (shouldContinue) {
                // set the executable bit on the newly installed appimage
                QFile installedFile{installedLocation};
                installedFile.setPermissions(QFile::ReadOwner | QFile::WriteOwner | QFile::ExeUser | QFile::ExeGroup| QFile::ExeOwner | QFile::ExeOther);
                // finally, if we've been asked to launch it, just launch it with appimaged wrap
                if (d->launch) {
                    QProcess appRunner;
                    qDebug() << "Going to do a launch" << d->appimagedLocation << "wrap" << installedLocation;
                    appRunner.start(d->appimagedLocation, {"wrap", installedLocation});
                    appRunner.waitForFinished();
                    if (appRunner.exitCode() != 0) {
                        qCritical() << appRunner.readAll();
                        Q_EMIT error(LaunchError);
                    }
                }
            }
        }
    }
    Q_EMIT done();
}
