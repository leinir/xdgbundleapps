# XDG Install Bundle App

This tool is a simple handler for apps distributed as bundles, with a knewstuff
configuration (knsrc) file for fetching them from the KDE Store. The intention
is to make supporting this as simple as possible, with as few dependencies as
possible, and as automatic as possible for the end user.

## Supported Bundle Formats

* AppImage (.AppImage) - Status: Mostly implemented
* Snap (.snap) - Status: Not begun **assistance welcome!**
* Flatpak Reference (.flatpakref) - Status: Not begun **assistance welcome!**

## Usage

The use of this tool is essentially a two-parter. First there is the tool itself,
`xdg-install-bundle-app`, and then there is the store integration point that is
the `storekdeapps.knsrc` file.

### xdg-install-bundle-app

To use the tool (once it has been built), simply run it and pass a file path to
it. Depending on the bundle type, there are varying levels of runtime requirements:

* AppImage: This will download and set up appimaged for you (which will integrate
  the bundles into your launcher and the like). This is itself an AppImage, and
  you can remove it using the `--remove` option mentioned below.
* Snap and Flatpak: You will need to have a functioning snapcraft or flatpak setup
  on your system (the tool will check the integrity of the setup and give you
  feedback on the state, and guide you to setting it up - the reason this is not
  done automatically is that it requires distribution intervention)

You can also use it to launch and remove bundle apps from your system, by using
the following flags:

* `--launch`: This will run the application contained in the bundle after ensuring
  that it is available on the system. If the bundle is already installed, it will
  simply be launched.
* `--allow-downgrade`: If you attempt to handle a bundle which is already installed
  with a newer version than what you are attempting to install, you will usually
  get an error. If you are certain you want to actually downgrade, you can pass
  this option to the tool to allow it to downgrade bundles.
* `--remove`: This will cause the tool to remove the bundle from the system. The
  exact result will depend on the type of bundle: AppImages are simply deleted,
  and Snaps and FlatPaks are uninstalled using their respective tools.

### KDE Store Integration

The KDE Store integration is done by way of a KNewStuff configuration file, or
knsrc file, which is installed alongside all the other knsrc files on your system.
That means that there are a couple of ways you can access it. You can either
run `knewstuff-dialog storekdeapps.knsrc` and it will show you the standard
KNewStuff Dialog tool with the KDE Store Apps listings, or you can use Discover,
which will show the KDE Store Apps in the Applications category.
