/*
 *   SPDX-FileCopyrightText: 2021 Dan Leinir Turthra Jensen <admin@leinir.dk>
 *
 *   SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#ifndef BUNDLEHANDLER_H
#define BUNDLEHANDLER_H

#include <QObject>

#include <memory>

class BundleHandler : public QObject
{
    Q_OBJECT
public:
    /**
     * Get an instance for the bundle file represented by the passed file.
     * This will set the filename on that instance, so you don't
     * necessarily need to do this yourself.
     * @param filename A file which we will hopefully be able to find a handler for
     * @return An instance of a BundleHandler which will handle this file, or null if no handler will take it
     */
    static BundleHandler* getInstanceForBundle(const QString &filename);

    explicit BundleHandler(QObject* parent = 0);
    virtual ~BundleHandler();

    void setBundleFile(QString filename);
    QString bundleFilename() const;
    void setShowProgress(bool showProgress);
    bool showProgress() const;
    void setShowCliProgress(bool showProgress);
    bool showCliProgress() const;
    void setLaunch(bool launch);
    bool launch() const;
    void setAllowDowngrade(bool allowDowngrade);
    bool allowDowngrade() const;
    void setRemove(bool remove);
    bool remove() const;

    virtual void start() = 0;

public Q_SLOTS:
    /**
     * Set the current progress state (updates the ui when options are set)
     * @param progress The current level of progress. 0 is unknown (equal to a "processsing" style state), 1 through 100 is a percentage state
     * @param description An optional text description of the current operation
     */
    virtual void setProgress(int progress, QString description = "");

Q_SIGNALS:
    /**
     * Emitted whether or not the bundle was successfully installed.
     * All subclasses must emit this.
     */
    void processCompleted();

private:
    class Private;
    std::unique_ptr<Private> d;
};

#endif // BUNDLEHANDLER_H
