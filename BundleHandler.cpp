/*
 *   SPDX-FileCopyrightText: 2021 Dan Leinir Turthra Jensen <admin@leinir.dk>
 *
 *   SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include "BundleHandler.h"
#include "AppImageHandler.h"

class BundleHandler::Private {
public:
    Private()
    {}
    QString filename;
    bool showProgress{false};
    bool showCliProgress{false};
    bool launch{false};
    bool allowDowngrade{false};
    bool remove{false};
    int currentProgress{0};
    QString currentProgressDescription;
};

BundleHandler* BundleHandler::getInstanceForBundle(const QString &filename)
{
    BundleHandler* handler = 0;

    // Use a failthrough list of possibilities found in our subclasses
    handler = AppImageHandler::getInstanceForBundle(filename);
    if(!handler) {
        // handler = SnapHandler::getInstanceForBundle(filename);
    }
    if(!handler) {
        // handler = FlatpakHandler::getInstanceForBundle(filename);
    }

    if(handler) {
        handler->setBundleFile(filename);
    }
    return handler;
}

BundleHandler::BundleHandler(QObject* parent)
    : QObject(parent)
    , d(new Private)
{
}

BundleHandler::~BundleHandler()
{
}

void BundleHandler::setBundleFile(QString filename)
{
    d->filename = filename;
}

QString BundleHandler::bundleFilename() const
{
    return d->filename;
}

void BundleHandler::setShowProgress(bool showProgress)
{
    d->showProgress = showProgress;
}

bool BundleHandler::showProgress() const
{
    return d->showProgress;
}

void BundleHandler::setShowCliProgress(bool showProgress)
{
    d->showCliProgress = showProgress;
}

bool BundleHandler::showCliProgress() const
{
    return d->showCliProgress;
}

void BundleHandler::setLaunch(bool launch)
{
    d->launch = launch;
}

bool BundleHandler::launch() const
{
    return d->launch;
}

void BundleHandler::setAllowDowngrade(bool allowDowngrade)
{
    d->allowDowngrade = allowDowngrade;
}

bool BundleHandler::allowDowngrade() const
{
    return d->allowDowngrade;
}

void BundleHandler::setRemove(bool remove)
{
    d->remove = remove;
}

bool BundleHandler::remove() const
{
    return d->remove;
}

void BundleHandler::setProgress(int progress, QString description)
{
    d->currentProgress = progress;
    d->currentProgressDescription = description;
    if(d->showProgress) {
        // update current UI progress
    }
    if(d->showCliProgress) {
        // update current CLI progress
    }
}
